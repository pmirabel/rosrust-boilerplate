# rosrust-boilerplate

rosrust-boilerplate with catkin and roslaunch integration

Go to your catkin workspace folder and type : 
``` 
export ROS_WORKSPACE=$(pwd) 
mkdir -p src
cd src
git clone git@gitlab.com:pmirabel/rosrust-boilerplate.git
cd $ROS_WORKSPACE
catkin_make_isolated
```

you can now  `roslaunch rosrust_package node1.launch`  